<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Diversion extends Model
{
    public static function getUserDetails($username,$password)
    {
    	$result = DB::table("diversions")->where("username",$username)->where("password",$password)->get(); 
    	return $result;
    } 

    public static function addHost($data)
    {
    	DB::table('diversions')->insert($data);
	}
}
