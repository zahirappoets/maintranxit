<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Diversion;

use GuzzleHttp\Client;

class ValidationController extends Controller
{
    public function validateLogin(Request $request)
    {
    	$username = $request->input('username');
    	$password = $request->input('accesskey');
    	$dataresult = Diversion::getUserDetails($username,$password);


    	if(count($dataresult))
    	{
    		$result['status'] = true;
    		$result['data'] = $dataresult;
			$result['setting'] = array();
		 
    	    $client = new Client();

	        $res = $client->request('GET', $dataresult[0]->serviceurl.'/initsetup', []);

            if($res->getStatusCode() == 200)
            {
                $json_data = json_decode($res->getBody(),true);

                if(!empty($json_data) && count($json_data))
                {
                    $result['setting'] = $json_data;
                }                
            }
    	}
    	else
    	{
			$result['status'] = false;
			$result['message'] = "Username or Password is not available";
    	}

    	return $result;
    }



    public function validateLogin1(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('accesskey');
        $dataresult = Diversion::getUserDetails($username,$password);

        if(count($dataresult))
        {
                $result['status'] = true;
                $result['data'] = $dataresult;
                $result['setting'] = array();

                $client = new Client();
                try{
                $res = $client->request('GET', $dataresult[0]->serviceurl.'/initsetup', []);

                if($res->getStatusCode() == 200)
                {
                $json_data = json_decode($res->getBody(),true);

                if(!empty($json_data) && count($json_data))
                {
                    $result['setting'] = $json_data;
                }
                }
                } catch (\Exception $e) {
                echo 'Caught response: ' . $e->getResponse()->getStatusCode();

                die;
                }
        }
        else
        {
                $result['status'] = false;
                $result['message'] = "Username or Password is not available";
        }

        return $result;
    }


    public function storeServiceDetails()
    { 
        $data = $_POST;    

        Diversion::addHost($data);         	
    } 


    public function validateDomain(Request $request)
    {
        $domain = $request->input('domain');
        $password = $request->input('accesskey');

        $data = Diversion::where('serviceurl',$domain)->where('password',$password)->first();

        $data_check = Diversion::where('password',$password)->first();


        if( strpos( $domain, 'localhost' ) !== false) {
            $result['status'] = true;
            $result['data'] = '';
        }
        elseif( strpos( $domain, '127.0.0.1' ) !== false) {
            $result['status'] = true;
            $result['data'] = '';
        }

        elseif(count($data)!=0)
        {
            $result['status'] = true;
            $result['data'] = $data;
        }
        else
        {
               if($data_check->serviceurl=='')
                {
                    $data_check->serviceurl = $domain;
                    $data_check->update();


                    $diversion_data = Diversion::find($diversion->id);
                    $result['status'] = true;
                    $result['data'] = $diversion_data;  
                }
                else
                {
                    $result['status'] = false;
                    $result['data'] = '';
                }
              
        }

        return $result;
    }

}