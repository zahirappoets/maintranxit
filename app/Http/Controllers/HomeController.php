<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diversion;

use Session;

use \GuzzleHttp\Client as GuzzleHttpClient;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function hostingList()
    {
        $Domains = Diversion::all(); 
        return view("hostinglist", compact('Domains'));
    }

    public function hostingAuto()
    {
        return view("hosting");
    }


    public function hostingCreate(Request $request)
    {
        $name = str_replace(" ","",strtolower($request->input('client_name')));//.substr(strtotime(date("Y-m-d h:i:s")),-4);  
        $conf = $name.".conf";

        $domain_name = $request->input("domain_name");


        $repo = "";
        $repo_folder_name = "";

        $product = $request->input("product");
        if($product == "schedule")
        {
            $repo = "https://zahirappoets:ayesha123%21@bitbucket.org/appoets/tranxit_schedule.git"; 
            $repo_folder_name = "tranxit_schedule";           
        }
        else if($product == "service")
        {
            $repo = "https://zahirappoets:ayesha123%21@bitbucket.org/appoets/tranxit_service_laravel.git"; 
            $repo_folder_name = "tranxit_service_laravel";           
        }
        else if($product == "foodie")
        {
            $repo = "https://zahirappoets:ayesha123%21@bitbucket.org/harapriya_appoets/foodie_web.git"; 
            $repo_folder_name = "foodie_web";
        }
 
        if($product && $repo && $name && $conf && $repo_folder_name)
        {
            echo "bash /var/www/html/test.sh $conf $name $domain_name $repo $repo_folder_name"; die;
            
            $key_output = exec("ssh -i serverfile/app root@main.venturedemos.com bash /var/www/html/test.sh $conf $name $domain_name $repo $repo_folder_name");
    
            $key_output = str_replace("1111","",$key_output);
 
            $data = array("client_name"=>$request->input('client_name'),"email"=>$request->input('email'),"product"=>$request->input('product'),"serviceurl"=>"http://".$request->input("domain_name"),"username"=>$name,"passport"=>$key_output);

            Diversion::addHost($data);

            $client = new GuzzleHttpClient(); 
            $response = $client->post('http://main.venturedemos.com/api/servicedetails',array('body'=>$data));
  
            $mailstatus = $this->sendClientMail($data);
            if($mailstatus)
                $mailmsg = " and Mail sent Successfully to ".$data['email'];
            else
                $mailmsg = " and Unable send an Email. Please send manually to ".$data['email'];

            return redirect('hostlist')->with("flash_success",'Hosted Successfully '.$mailmsg);
        }
        else
        {
            echo "Data are missing to Create Host for ".ucwords($product);
        }
    }


    public function sendClientMail($data)
    {
        $to = $data['email'];
        $from = "sales@appoets.com";
        $subject = "Access Details for Product";
        $message = $this->htmlContent($data);

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();

        if(mail($to, $subject, $message, $headers))
            return 1;
        else
            return 0;
    }

    public function htmlContent($data)
    {
        $htmlcontent = "Hi ".$data['client_name']."<br><br>";
        $htmlcontent .= "Please use below credential to check the Product. <br><br>";

        $htmlcontent .= "URL :".$data['serviceurl']."<br><br>";

        $htmlcontent .= "Admin URL:".$data['serviceurl']."/admin/login<br><br>";


        if($data['product'] == "service")
            $htmlcontent .= "Username : admin@xuber.com <br><br>";
        else if($data['product'] == "schedule")
            $htmlcontent .= "Username : admin@tranxit.com <br><br>";
        else if($data['product'] == "foodie")
            $htmlcontent .= "Username : admin@foodie.com <br><br>";

        $htmlcontent .= "Password : 123456 <br>";



        return $htmlcontent;
    }
}
