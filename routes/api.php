<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login' , 'ValidationController@validateLogin');
Route::post('/login1' , 'ValidationController@validateLogin1');
Route::post('/servicedetails','ValidationController@storeServiceDetails');


//Route::get('/test','ValidationController@testing'); 

Route::post('/check/domain','ValidationController@validateDomain'); 