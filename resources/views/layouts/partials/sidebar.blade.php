<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
        <div class="main-menu-content">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">                 
                <li class=" nav-item">
                    <a href="#"><i class="fa fa-flag"></i><span data-i18n="" class="menu-title">Dashboard</a>
                </li>
                <li class=" nav-item"><a href="#"><i class="fa fa-line-chart"></i><span data-i18n="" class="menu-title">Host</span></a>
                    <ul class="menu-content">
                        <li><a href="/hostlist" class="menu-item">Hosted List</a></li>
                        <li><a href="/autohost" class="menu-item">New Host</a></li>
                    </ul>
                </li>                      
            </ul>
        </div>
    </div>