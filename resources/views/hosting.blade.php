@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Hosting</h3>
    </div>
    <div class="card-body collapse in">
    <div class="card-block">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default"> 

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card-body collapse in">
                        <div class="card-block">
                            <form role="form" method="POST" id="demohosting" action="hosting/create">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="name">Client Name</label>

                                    <input id="client_name" name="client_name" type="text" class="form-control" value="" required autofocus> 
                                </div>

                                <div class="form-group">
                                    <label for="name">Domain Name</label>

                                    <input id="domain_name" name="domain_name" type="text" class="form-control" value="" required> 
                                </div>

                                <div class="form-group">
                                    <label for="name">Email</label>

                                    <input id="email" name="email" type="email" class="form-control" value="" required> 
                                </div>

                                
                                <div class="form-group">
                                    <label for="name">Product</label>

                                    <select id="product" name="product" class="form-control" value="" required> 
                                        <option value="">--Select Product--</option>
                                        <option value="schedule">Tranxit Schedule</option>
                                        <option value="service">Tranxit Service</option>
                                        <option value="foodie">Foodie</option>
                                    </select>
                                </div>
                                 

                                 <div class="form-group">
                                    <a id="clear" class="btn btn-warning mr-1">
                                        <i class="ft-x"></i> Clear
                                    </a>
                                    <button name="servername" id="servername" onclick="hostingcreate()" class="btn btn-primary">
                                        <i class="fa fa-check-square-o"></i> Create
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    function hostingcreate()
    {
        return false;
        if($.trim($("#serv").val()) == "")
        {
            alert("Please Enter Demo Server Name");
            return false;
        }

        $("#servername").val($.trim($("#serv").val()));
        $("#demohosting").submit();
    }
</script>   
@endsection