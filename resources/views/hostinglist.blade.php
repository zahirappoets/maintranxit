@extends('layouts.app')

@section('content')
<!-- File export table -->
<div class="row file">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">            
            @include('include.alerts')
                <h4 class="card-title">Hosting List</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>                       
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard table-responsive">
                    <table class="table table-striped table-bordered file-export">
                        <thead>
                            <tr>
                                <th>S. No</th>
                                <th>Client Name</th>
                                <th>Email</th>
                                <th>Product</th>
                                <th>Domain Name</th>  
                                <th>Username</th>
                                <th>Passport</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($Domains as $User)
                                <tr>
                                    <td>{{$User->id}}</td>
                                    <td>{{$User->client_name}}</td>
                                    <td>{{$User->email}}</td>                                     
                                    <td>{{$User->product}}</td>
                                    <td>{{$User->serviceurl}}</td> 
                                    <td>{{$User->username}}</td>
                                    <td>{{$User->passport}}</td> 
                                </tr>
                            @empty
                                <tr><td colspan="10">No Records Found</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection